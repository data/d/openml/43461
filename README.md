# OpenML dataset: Tamilnadu-Crop-production

https://www.openml.org/d/43461

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Agriculture crop production in Tamilnadu, India
Content
This Dataset Describes the Agriculture Crops Production in Tamilnadu, India. This is from https://data.gov.in/ fully Licensed
Acknowledgements
This is done during the Internship at TactLabs.Thanks to Raja sir and Vaishnavi for helping me with this
The target variable can be Production, however it is incomplete

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43461) of an [OpenML dataset](https://www.openml.org/d/43461). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43461/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43461/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43461/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

